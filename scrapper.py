from selenium import webdriver
from tqdm import tqdm
import pandas as pd
import time
import numpy as np

options = webdriver.ChromeOptions()

#unable this item to get browser running
options.add_argument("headless")

DRIVER_PATH = './chromedriver'
driver = webdriver.Chrome(executable_path=DRIVER_PATH, options=options)
driver.get('https://google.com')

def get_categories(item_name,delay=5):
    driver.get(f"https://tiendanaranja.com.py/busqueda?controller=search&s={item_name}")
    time.sleep(delay/2)
    
    product_list = driver.find_elements_by_class_name('CardProduct')
    category_1 = category_2 = None
    
    if (len(product_list) > 0):
        product_list[0].click()
        time.sleep(delay/2)
        
        breadcrumb_itens = driver.find_elements_by_class_name('breadcrumb-item')
        
        if(len(breadcrumb_itens) > 0):
            category_1 = breadcrumb_itens[1].find_element_by_tag_name("span").text
            category_2 = breadcrumb_itens[2].find_element_by_tag_name("span").text

    return (category_1, category_2)

def extract_categories(list_items_to_search_for):
    categories = []
    for product in tqdm(list_items_to_search_for):
        result = get_categories(product)
        categories.append(result)

    df = pd.DataFrame(categories, columns=["category","subcategory"])
    df["item_name"] = list_items_to_search_for
    
    return df